/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examenrec02;

/**
 *
 * @author andsa
 */
public class Nomina {
    private int numRecibo;
    private String nombre; 
    private int puesto;
    private int nivel;
    private int diasT;
    
    //Constructores
    public Nomina(){
        this.nombre="";
        this.numRecibo=0;
        this.nivel=0;
        this.puesto=0;
        this.diasT=0;
    }
    
    public Nomina(int numRecibo, String nombre, int puesto, int nivel, int diasT){
        this.nombre=nombre;
        this.diasT=diasT;
        this.nivel=nivel;
        this.puesto=puesto;
        this.numRecibo=numRecibo;
    }
    
    public Nomina(Nomina otro){
        this.nombre=otro.nombre;
        this.diasT=otro.diasT;
        this.nivel=otro.nivel;
        this.puesto=otro.puesto;
        this.numRecibo=otro.numRecibo;
    }
    
    //Setter & Getter

    public int getNumRecibo() {
        return numRecibo;
    }

    public void setNumRecibo(int numRecibo) {
        this.numRecibo = numRecibo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getPuesto() {
        return puesto;
    }

    public void setPuesto(int puesto) {
        this.puesto = puesto;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public int getDiasT() {
        return diasT;
    }

    public void setDiasT(int diasT) {
        this.diasT = diasT;
    }
    
    //métodos de comportamiento
    
     public float calcularPago(){
         float pago=0.0f;
         if (puesto == 1){
            pago = 100;
             pago=pago*diasT;
        } else if (puesto == 2) {
            pago = 200;
             pago=pago*diasT;
        } else if (puesto == 3) {
            pago = 300;
             pago=pago*diasT;
        } 
         return pago;
                 }
     
     public float calcularImpuesto(){
         float impuesto=0.0f;
         if (nivel == 1) {
            impuesto = this.calcularPago() * 0.05f;
        } else if (nivel == 2) {
            impuesto = this.calcularPago() * 0.03f; 
        } 
         return impuesto;
                 }
     

     public float calcularTotalPagar(){
         float totalPagar=0.0f;
         totalPagar=this.calcularPago()-this.calcularImpuesto();
         return totalPagar;
         }
     }

     
     


